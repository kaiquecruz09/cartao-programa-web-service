<?php

class Xml{
	private $xml;
	private $tab = 0;

	public function __construct($version="1.0", $encode="utf-8"){  //iso-8859-1
		$this->xml = "<?xml version='$version' encoding='$encode' ?>\n";
	}
	
	private function recuo(){
		for($i=0; $i< $this->tab; $i++)
			$this->xml .= "\t";
	}

	public function openTag($name){
		$this->recuo();
		$this->xml .= "<".$name.">\n";
		$this->tab++;
	}
	
	public function closeTag($name){
		$this->tab--;
		$this->recuo();
		$this->xml .= "</".$name.">\n";
	}
	
	public function addTag($name, $value){
		$this->recuo();
		$this->xml .= "<".$name.">".$value."</".$name.">\n";
	}
	
	public function addValue($value){
		$this->xml .= $value."\n";
	}
	
	public function __toString(){
		return $this->xml;
	}
}

?>