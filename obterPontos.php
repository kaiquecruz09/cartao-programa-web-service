<?php

function getPoints($id){
	require_once("Xml.Class.php");
	require_once("banco.php");

	$xml = new Xml();

	$xml->openTag("points");

	$errorForm = 0;

	if($id == ''){
		$errorForm = 1;
	}
	else{
		$cMarkerB =  mysql_query("SELECT description, 
			                            arrival, 
			                            leaving,
			                            l.lat,
			                            l.lng 
								 FROM card AS c, 
								      map AS m, 
								      markerBase AS mb,
								      latLng AS l
								 WHERE c.id=$id AND 
								 	   c.idMap=m.id AND 
								 	   c.id=mb.idCard AND
								 	   mb.idLatLng=l.id"
			        );

		$cMarkerG = mysql_query("SELECT description, 
			                            lat,
			                            lng 
								 FROM markerGlobal"
			        );

		$cMarkerR = mysql_query("SELECT mr.id,
										l.lat,
			                            l.lng 
								 FROM markerRoute AS mr,
								      latLng AS l
								 WHERE mr.idLatLng=l.id"
			        );


		if(mysql_num_rows($cMarkerB) > 0){
			while($linha = mysql_fetch_object($cMarkerB)){
				$xml->openTag("point");
					$xml->addTag('type', "basePoint");
					$xml->addTag('description', $linha->description);
					$xml->addTag('lat', $linha->lat);
					$xml->addTag('lng', $linha->lng);
					$xml->addTag('arrival', $linha->arrival);
					$xml->addTag('leaving', $linha->leaving);
				$xml->closeTag("point");
				$errorMarkerB = 0;
		    }
		}
		else{
			$errorMarkerB = 1;
			//$msg_error = "O ID fornecido nao esta cadastrado no banco.";
		}


		if(mysql_num_rows($cMarkerG) > 0){
			while($linha = mysql_fetch_object($cMarkerG)){
				$xml->openTag("point");
					$xml->addTag('type', "globalPoint");
					$xml->addTag('description', $linha->description);
					$xml->addTag('lat', $linha->lat);
					$xml->addTag('lng', $linha->lng);
					$xml->addTag('arrival', "-");
					$xml->addTag('leaving', "-");
				$xml->closeTag("point");
				$errorMarkerG = 0;
			}
		}
		else{
			$errorMarkerG = 1;
			//$msg_error = "O ID fornecido nao esta cadastrado no banco.";
		}


		if(mysql_num_rows($cMarkerR) > 0){
			while($linha = mysql_fetch_object($cMarkerR)){
				$xml->openTag("point");
					$xml->addTag('type', "routePoint");
					$xml->addTag('description', $linha->description);
					$xml->addTag('lat', $linha->lat);
					$xml->addTag('lng', $linha->lng);
					$xml->addTag('arrival', "-");
					$xml->addTag('leaving', "-");
				$xml->closeTag("point");
				$errorMarkerR = 0;
			}
		}
		else{
			$errorMarkerR = 1;
			//$msg_error = "O ID fornecido nao esta cadastrado no banco.";
		}
	}



	if($errorForm != 0){
		$xml->addTag("error", $errorForm);
		$xml->addTag("msg_error", "Formulário incompleto");
	}
	elseif($errorMarkerB == 1 && $errorMarkerG == 1 && $errorMarkerR == 1){
			$xml->addTag("error", $errorMarker);
			$xml->addTag("msg_error", "Não há pontos para este cartão programa");
	}
	else {
		$xml->closeTag("points");

		//return $xml;

		//Para enviar somente o XML:
		echo $xml;
 	}

}

?>