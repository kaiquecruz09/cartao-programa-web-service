
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 04/11/2014 às 03:00:40
-- Versão do Servidor: 10.0.12-MariaDB
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u969454758_pmpa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  `areaDescription` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `routeDescription` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardDescription` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `idMap` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_card_map1` (`idMap`),
  KEY `fk_card_user1` (`idUser`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `card`
--

INSERT INTO `card` (`id`, `start`, `finish`, `areaDescription`, `routeDescription`, `cardDescription`, `created`, `idMap`, `idUser`) VALUES
(1, '2014-11-03 20:30:00', '2014-11-03 23:00:00', 'Campus Básico da UFPA', 'Contorno no Campus Básico da UFPA', 'Guarnição nº: 9999', '2014-11-03 20:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `incident`
--

CREATE TABLE IF NOT EXISTS `incident` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLatLng` int(11) NOT NULL,
  `idCard` int(11) NOT NULL,
  `attended` tinyint(1) NOT NULL,
  `timeStamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_incident_latLng_id_idx` (`idLatLng`),
  KEY `fk_incident_card_id_idx` (`idCard`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `latLng`
--

CREATE TABLE IF NOT EXISTS `latLng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `latLng`
--

INSERT INTO `latLng` (`id`, `lat`, `lng`) VALUES
(1, -1.45601, -48.469),
(2, -1.4743, -48.4558),
(3, -1.47684, -48.4555),
(4, -1.47784, -48.4577),
(5, -1.47634, -48.4584),
(6, -1.47561, -48.4583),
(7, -1.4734, -48.4586),
(8, -1.47216, -48.4583),
(9, -1.47276, -48.4559),
(10, -1.4743, -48.4558);

-- --------------------------------------------------------

--
-- Estrutura da tabela `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeStamp` datetime NOT NULL,
  `idLogEvent` int(11) NOT NULL,
  `idLatLng` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `idCard` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_logEvent_idx` (`idLogEvent`),
  KEY `fk_log_LatLng_idx` (`idLatLng`),
  KEY `fk_log_card1_idx` (`idCard`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `logEvent`
--

CREATE TABLE IF NOT EXISTS `logEvent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mancha`
--

CREATE TABLE IF NOT EXISTS `mancha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `timeStamp` datetime DEFAULT NULL,
  `idManchaType` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mancha_manchaType1_idx` (`idManchaType`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `mancha`
--

INSERT INTO `mancha` (`id`, `lat`, `lng`, `timeStamp`, `idManchaType`) VALUES
(1, -1.45601, -48.469, '2014-10-30 21:54:10', 1),
(2, -1.47348, -48.4532, '2014-10-28 22:30:00', 1),
(3, -1.47356, -48.4526, '2014-10-29 22:40:32', 1),
(4, -1.47314, -48.4532, '2014-11-26 21:15:55', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `manchaType`
--

CREATE TABLE IF NOT EXISTS `manchaType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `manchaType`
--

INSERT INTO `manchaType` (`id`, `type`, `color`) VALUES
(1, 'assalto', 'red'),
(2, 'sequestro', 'yellow');

-- --------------------------------------------------------

--
-- Estrutura da tabela `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zoom` int(55) NOT NULL,
  `idLatlng` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_map_latlng1` (`idLatlng`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `map`
--

INSERT INTO `map` (`id`, `zoom`, `idLatlng`) VALUES
(1, 50, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `markerBase`
--

CREATE TABLE IF NOT EXISTS `markerBase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrival` time DEFAULT NULL,
  `leaving` time DEFAULT NULL,
  `idLatLng` int(11) NOT NULL,
  `idCard` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_marker_latlng1` (`idLatLng`),
  KEY `fk_marker_map1` (`idCard`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `markerBase`
--

INSERT INTO `markerBase` (`id`, `description`, `arrival`, `leaving`, `idLatLng`, `idCard`) VALUES
(1, 'Prefeitura da UFPA', '22:00:00', '22:15:00', 5, 1),
(2, '2º Portão UFPA', '22:50:00', '23:00:00', 9, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `markerGlobal`
--

CREATE TABLE IF NOT EXISTS `markerGlobal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `markerGlobal`
--

INSERT INTO `markerGlobal` (`id`, `description`, `lat`, `lng`) VALUES
(1, 'Restaurante Universitário da UFPA', -1.477875, -48.458167),
(2, 'Ginásio de Esportes', -1.473209, -48.455668);

-- --------------------------------------------------------

--
-- Estrutura da tabela `markerRoute`
--

CREATE TABLE IF NOT EXISTS `markerRoute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLatLng` int(11) NOT NULL,
  `idCard` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_markerRoute_latLng1_idx` (`idLatLng`),
  KEY `fk_markerRoute_card1_idx` (`idCard`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `markerRoute`
--

INSERT INTO `markerRoute` (`id`, `idLatLng`, `idCard`) VALUES
(1, 2, 1),
(2, 3, 1),
(3, 4, 1),
(4, 5, 1),
(5, 6, 1),
(6, 7, 1),
(7, 8, 1),
(8, 9, 1),
(9, 10, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idNotificationType` int(11) NOT NULL,
  `title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `idCard` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notificiation_notificationType1_idx` (`idNotificationType`),
  KEY `fk_notificiation_card1_idx` (`idCard`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `notification`
--

INSERT INTO `notification` (`id`, `idNotificationType`, `title`, `description`, `idCard`) VALUES
(1, 3, 'Via obstruída', 'Acidente entre dois carros em frente ao CAPACIT.', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificationType`
--

CREATE TABLE IF NOT EXISTS `notificationType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `notificationType`
--

INSERT INTO `notificationType` (`id`, `description`) VALUES
(1, 'Congestionamento'),
(2, 'Fuga'),
(3, 'Acidente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `polygon`
--

CREATE TABLE IF NOT EXISTS `polygon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMap` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_polygon_map1` (`idMap`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `polygonLatLng`
--

CREATE TABLE IF NOT EXISTS `polygonLatLng` (
  `idPolygon` int(11) NOT NULL,
  `idLatLng` int(11) NOT NULL,
  PRIMARY KEY (`idPolygon`,`idLatLng`),
  KEY `fk_polygon_has_latLng_latLng1` (`idLatLng`),
  KEY `fk_polygon_has_latLng_polygon1` (`idPolygon`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `polyline`
--

CREATE TABLE IF NOT EXISTS `polyline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMap` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_polyline_map1` (`idMap`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `polylineLatLng`
--

CREATE TABLE IF NOT EXISTS `polylineLatLng` (
  `idPolyline` int(11) NOT NULL,
  `idLatLng` int(11) NOT NULL,
  PRIMARY KEY (`idPolyline`,`idLatLng`),
  KEY `fk_polyline_has_latLng_latLng1` (`idLatLng`),
  KEY `fk_polyline_has_latLng_polyline1` (`idPolyline`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, '22222222222', '123456');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
