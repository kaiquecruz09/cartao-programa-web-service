<meta charset="UTF-8" />
<html>
<head>
<style type="text/css">
	body{
		background-image: url("back.jpg");
		font-family: Calibri;
	}
	#forms{
		margin-top:-5px;
		border:1px solid;
		padding:13px;
		height:215px;
	}
</style>

<?php
function ramdom_pos(){
	$pos_int = rand(-50,50);
	$pos_dec = rand(100000,999999);
	return $pos_int.".".$pos_dec;		
}
function ramdom_date(){
	$d = rand(0,30);
	$m = rand(1,12);
	$a = rand(2014,2016);
	if($d<10) $d = "0".$d;
	if($m<10) $m = "0".$m;
	if($a<10) $a = "0".$a;
	return $a."-".$m."-".$d;
}
function ramdom_time(){
	$h = rand(1,24);
	$m = rand(1,59);
	$s = rand(1,59);
	if($h<10) $h = "0".$h;
	if($m<10) $m = "0".$m;
	if($s<10) $s = "0".$s;
	return $h.":".$m.":".$s;
}
function ramdom_ip(){
	return "10.1.".rand(0,255).".".rand(0,255);
}
?>

<script type="text/javascript">
function formata_data(obj){
	if(obj.value.length == 4 || obj.value.length == 7){
		obj.value += "-";
	}
}
function formata_hora(obj){
	if(obj.value.length == 2 || obj.value.length == 5){
		obj.value += ":";
	}
}
</script>

</head>

<body>

<table width="95%"><tr><td valign="top"width="18%">

<div id="forms">
	<form action="xmlObterCP.php" target="result" method="GET">
		<legend>Serviço: <br><b>obterCartaoPrograma</b></legend><br>
		CPF <input type="number" name="cpf" value="22222222222" style="width:70%;" maxlength="11" /> <br><br>
		Senha <input type="password" name="pass" value="123456" style="width:70%;" /> <br><br>
		<input type="submit" value="Testar">
	</form>
</div>

</td><td valign="top" width="14%">

<div id="forms">
	<form action="xmlObterPontos.php" target="result" method="GET">
		<legend>Serviço: <br><b>obterPontos</b></legend>
		ID do CP <input type="number" name="idCard" value="1" style="width:40%;" /> <br>
		<input type="submit" value="Testar">
	</form>
</div>

</td><td valign="top" width="16%">

<div id="forms">
	<form action="xmlObterNotificacoes.php" target="result" method="GET">
		<legend>Serviço: <br><b>obterNotificacoes</b></legend>
		ID do CP <input type="number" name="idCard" style="width:30%;" value="1" /> <br><br>
		ID da &uacute;ltima notifica&ccedil;&atilde;o <input type="number" name="idLast" style="width:30%;" value="0" /> <br>
		<input type="submit" value="Testar">
	</form>
</div>

</td><td valign="top" width="21%">

<div id="forms">
	<form action="xmlInformarOcorrencia.php" target="result" method="GET">
		<legend>Serviço: <br><b>InformarOcorrencia</b></legend>
		Lat <input type="text" name="lat" value="<?=ramdom_pos();?>" style="width:50%;" /> <br>
		Lng <input type="text" name="lng" value="<?=ramdom_pos();?>" style="width:50%;" /> <br>
		ID do CP <input type="number" name="idCard" value="1" style="width:20%;" /> <br>
		Data <input type="text" name="data" value="<?=ramdom_date();?>" style="width:75%;" onKeyPress="formata_data(this)" maxlength="8" /> <br>
		Hora <input type="text" name="hora" value="<?=ramdom_time();?>" style="width:75%;" onKeyPress="formata_hora(this)" maxlength="8" /> <br>
		Atendida: <input type="radio" value="1" name="ok" /> Sim 
		          <input type="radio" value="0" name="ok" checked /> Não <br>
		<input type="submit" value="Testar">
	</form>
</div>

</td><td valign="top" width="21%">

<div id="forms">
	<form action="xmlEncerrarTurno.php" target="result" method="GET">
		<legend>Serviço: <br><b>encerrarTurno</b></legend>


		Lat <input type="text" name="lat" value="<?=ramdom_pos();?>" style="width:50%;" /> <br>
		Lng <input type="text" name="lng" value="<?=ramdom_pos();?>" style="width:50%;" /> <br>
		ID do CP <input type="number" name="idCard" value="1" style="width:20%;" /> <br>
		Data <input type="text" name="data" value="<?=ramdom_date();?>" style="width:75%;" onKeyPress="formata_data(this)" maxlength="8" /> <br>
		Hora <input type="text" name="hora" value="<?=ramdom_time();?>" style="width:75%;" onKeyPress="formata_hora(this)" maxlength="8" /> <br>
		IP: <input type="text" value="<?=ramdom_ip();?>" name="ip" style="width:50%;" /> <br>
		<input type="submit" value="Testar">
	</form>
</div>

</td></tr></table>

</body>
</html>