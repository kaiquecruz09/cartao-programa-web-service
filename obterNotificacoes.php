<?php

function getNotificacoes($idCard, $idLast){
	require_once("Xml.Class.php");
	require_once("banco.php");

	$xml = new Xml();

	$xml->openTag("notifications");

	if($idCard == '' || $idLast == ''){
		$error = 1;
		$msg_error = "Formulario incompleto.";
	}
	else{
		$consulta = mysql_query("SELECT n.id AS lastID,
										n.title,
			                            n.description AS notification,
			                            nt.description AS ntDescription
								 FROM notificationType AS nt, 
								      notification AS n,
								      card AS c
								 WHERE c.id=$idCard AND
								       c.id=n.idCard AND
								       n.id>$idLast AND
								       n.idNotificationType=nt.id");


		if(mysql_num_rows($consulta) > 0){
			while($linha = mysql_fetch_object($consulta)){
				$xml->openTag("notification");
					$xml->addTag('lastID', $linha->lastID);
					$xml->addTag('title', $linha->title);
					$xml->addTag('description', $linha->notification);
					$xml->addTag('ntDescription', $linha->ntDescription);
				$xml->closeTag("notification");
			}
			$error = 0;
		}
		else{
			$error = 2;
			$msg_error = "Não há novas notificações";
		}
	}

	if($error != 0){
		$xml->addTag("error", $error);
		$xml->addTag("msg_error", $msg_error);
	}

	$xml->closeTag("notifications");

	//return $xml;

	//Para enviar somente o XML:
	echo $xml;
}

?>