<?php

function encerrTurno($idCard, $lat, $lng, $data, $hora, $ip){
	require_once("Xml.Class.php");
	require_once("banco.php");

	$xml = new Xml();

	$xml->openTag("encerrar");

	if($idCard == '' || $lat == '' || $lng == '' || $data == '' || $hora == '' || $ip == ''){
		$error = 1;
		$msg_error = "Formulario incompleto.";
	}
	else{
		$ins1 = mysql_query("INSERT INTO latLng VALUES ('', $lat, $lng)");

		$xml->addTag("lat", $lat);
		$xml->addTag("lng", $lng);

		//Capturando o último id da tabela latlng, que será a chave estrangeira idlatlng da tabela incident
		$consulta = mysql_query("SELECT id FROM latLng ORDER BY id DESC LIMIT 1");
		$last_id = mysql_fetch_array($consulta);
		$idlatlng = $last_id['id'];

		/*
		$consulta2 = mysql_query("SELECT id FROM logEvent ORDER BY id DESC LIMIT 1");
		$last_id = mysql_fetch_array($consulta2);
		$idLogEvent = $last_id['id'];
        */

		$datetime = $data." ".$hora;
	
		$ins2 = mysql_query("INSERT INTO log VALUES ('', '$datetime', 0, $idlatlng, '$ip', $idCard)");

		$xml->addTag("timestamp", $datetime);
		$xml->addTag("idLogEvent", $idLogEvent);
		$xml->addTag("idCard", $idCard);
		$xml->addTag("idlatlng", $idlatlng);
		$xml->addTag("ip", $ip);

		if($ins1 && $ins2){
			$error = 0;
			$xml->addTag("ack", "1");
		}
		else{
			$error = 2;
			$msg_error = "Erro ao inserir registros no banco de dados.";
		}
	}

	if($error != 0){
		$xml->addTag("error", $error);
		$xml->addTag("msg_error", $msg_error);
	}

	$xml->closeTag("encerrar");

	return $xml;

	//Para enviar somente o XML:
	//echo $xml;
}

?>